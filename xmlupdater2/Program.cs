﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace xmlupdater2
{
    class Program
    {
        static void Main(string[] args)
        {
            string setting_path = Directory.GetCurrentDirectory() + "\\setting.xml";
            string lr2setting_path = "";
            int LR2ID = 0;
            bool seisuuka = false;
            double recommend_value = 0;
            bool autoswitch = false;

            try
            {
                XElement xml_f = XElement.Load(setting_path);
                XElement info_f = (from item in xml_f.Elements("setting") select item).First();
                lr2setting_path = info_f.Element("lr2setting_path").Value;
                LR2ID = int.Parse(info_f.Element("LR2ID").Value);
                autoswitch = bool.Parse(info_f.Element("autoswitch").Value);
            }

            catch
            {
                Console.WriteLine("設定を読み込めませんでした 入力モードに移行します");
            }

            if (autoswitch == false)
            {
                Console.WriteLine("lr2helper-setting.xmlのパスを指定して下さい(入力後Enterで進む)");

                lr2setting_path = Console.ReadLine();

                do
                {
                    Console.WriteLine("LR2IDを入力して下さい(入力後Enterで進む)");

                    string line = Console.ReadLine();

                    //入力された文字が整数かをチェックして設定する
                    seisuuka = int.TryParse(line, out LR2ID);

                    //整数に変換できない場合、エラーメッセージを表示
                    if (!seisuuka)
                    {
                        Console.WriteLine("入力された文字が不正です。半角の数字で整数を入力してください。");
                    }
                } while (!seisuuka);

                seisuuka = false;

                do
                {
                    //コンソールから何頭立てかを入力させる
                    Console.WriteLine("次回から自動でデータ取得する場合はtrue、しない場合はfalseを入力して下さい(入力後Enterで進む)");

                    string line = Console.ReadLine();

                    //入力された文字がBoolかをチェックして設定する
                    seisuuka = bool.TryParse(line, out autoswitch);

                    //Boolに変換できない場合、エラーメッセージを表示
                    if (!seisuuka)
                    {
                        Console.WriteLine("入力された文字が不正です。");
                    }
                } while (!seisuuka);

                XElement xml_m = XElement.Load(setting_path);
                XElement info_m = (from item in xml_m.Elements("setting") select item).First();
                info_m.Element("LR2ID").Value = $"{LR2ID}";
                info_m.Element("lr2setting_path").Value = lr2setting_path;
                info_m.Element("autoswitch").Value = $"{autoswitch}";
                xml_m.Save(setting_path);
                Console.WriteLine("XMLに設定を保存しました");
            }

            if (autoswitch == true)
            {
                Console.WriteLine("設定より自動で取得します");
            }

            ChromeOptions chromeOptions = new ChromeOptions();

            ChromeDriverService chromeService = ChromeDriverService.CreateDefaultService();
            // 非表示オプション
            chromeService.HideCommandPromptWindow = true;
            chromeOptions.AddArguments("--headless");

            Console.WriteLine("ChromeDriverオブジェクトを生成します");
            // ChromeDriverオブジェクトを生成します。
            var chrome = new ChromeDriver(chromeService, chromeOptions);
            Console.WriteLine("ChromeDriverオブジェクト生成完了");
            // Waitを作成
            WebDriverWait wait = new WebDriverWait(chrome, TimeSpan.FromSeconds(10));


            var recommend_update = $"http://pasta-soft.com/bms/recommend.php?id={LR2ID}";

            Console.WriteLine("リコメンドを更新します");

            chrome.Url = recommend_update;

            // 更新待機
            wait.Until(c => c.FindElement(By.CssSelector("#recommendedtab")));

            Console.WriteLine("リコメンド更新完了");

            var IRmypage = $"http://www.dream-pro.info/~lavalse/LR2IR/search.cgi?mode=mypage&playerid={LR2ID}";

            Console.WriteLine("IR段位を取得します");

            chrome.Url = IRmypage;

            // 更新待機
            wait.Until(c => c.FindElement(By.XPath("/html/body/div[1]/div/div/table[1]/tbody/tr[3]/td")));

            // xpathで指定
            string IRname = chrome.FindElementByXPath("/html/body/div[1]/div/div/table[1]/tbody/tr[1]/td").Text;
            string IRrank = chrome.FindElementByXPath("/html/body/div[1]/div/div/table[1]/tbody/tr[3]/td").Text;

            Console.WriteLine("IR段位取得完了");

            var recommend_mypage = $"http://walkure.net/hakkyou/recommended_mypage.html?playerid={LR2ID}";

            Console.WriteLine("リコメンドを取得します");

            chrome.Url = recommend_mypage;

            // 読み込み終わるまで値取得できないので
            wait.Until(c => c.FindElement(By.CssSelector("#tab-recommended")));

            Thread.Sleep(500);

            // idで指定
            string recommend_message = chrome.FindElementByCssSelector("#jitsuryoku").Text;

            string target_top = "★";

            //暫定 値加工 良くないコード
            string recommend_text = (recommend_message.Substring(recommend_message.IndexOf(target_top) + 1, 5));

            if (double.TryParse(recommend_text, out recommend_value))
            {
                Console.WriteLine("リコメンド取得完了");
            }
            else
            {
                Console.WriteLine("リコメンド取得失敗");
            }


            chrome.Quit();

            Console.WriteLine($"以下に取得結果を表示します");
            Console.WriteLine($"name:{IRname}");
            Console.WriteLine($"ID:{LR2ID}");
            Console.WriteLine($"段位:{IRrank}");
            Console.WriteLine($"リコメンド:{recommend_value}");

            Console.WriteLine("続いてxmlにデータを書き込みます");

            XElement xml = XElement.Load(lr2setting_path);
            XElement info = (from item in xml.Elements("setting") select item).First();
            info.Element("rank").Value = $"{IRrank}";
            info.Element("recommend").Value = $"{recommend_value}";
            info.Element("LR2ID").Value = $"{LR2ID}";
            info.Element("LR2name").Value = $"{IRname}";

            xml.Save(lr2setting_path);
            Console.WriteLine($"更新完了 5秒後に終了します");
            Thread.Sleep(5000);
        }
    }
}
